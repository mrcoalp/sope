#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#define MAXLINE 1028
#define READ 0
#define WRITE 1

int main(int argc, char** argv){
	int i, j, pipeNum, numOfFiles = argc-3, fd[numOfFiles][2][2], execTime = atoi(argv[1]), timeTemp = 5, posFile=3, running=1;
	char* word = (char*)malloc(sizeof(char));
	char line[MAXLINE];
	pid_t pidFiles[3][numOfFiles]; //array de pid para guardar todos os pid
	clock_t start = clock(), now;
	
	word = argv[2];

	for(j=0; j<MAXLINE; j++) //limpa o line da lixeira
		line[j]=0;
		
	printf("\n============================================");
	
	if(argc < 4){
		printf("\nMissing information! Usage: %s <tempo> <palavra> <files>\nProcess will end.", argv[0]);
		printf("\n============================================\n\n");
		return -1;
	}
	
	for(i=0;i<numOfFiles;i++)
		if( access(argv[3+i], F_OK) == -1 ){ //se não encontrou o ficheiro
			printf("\nFile not found: %s. Process will end.", argv[3+i]);
			printf("\n============================================\n\n");
			return -1;
		}

	printf("\nInformation inserted:\nNumber of files entered: %d\nExecution Time Limit: %d\nWord to find: \"%s\"\n", numOfFiles, execTime, word);
	
	for(i=0;i<numOfFiles;i++) //cria todos os pipes necessários
		for(j=0;j<2;j++)
			if(pipe(fd[i][j])<0){
				perror("Creation pipe failed.");
				return -1;
			}
			
	printf("\nFiles are being monitored...\n");

	for(i = 0; i < numOfFiles; i++){

		//Novo processo
		pidFiles[0][i] = fork();
		if(pidFiles[0][i]<0){
			perror("Fork error.\n");
			return -1;
		}

		if(pidFiles[0][i] == 0){//primeiro processo filho, realiza o tail
			pipeNum=i;
			int l,m,n;
			for(l=0;l<numOfFiles;l++)	//close dos pipes abertos
				for(m=0;m<2;m++) for(n=0;n<2;n++)
					if(!(l==pipeNum && m==0 && n==1))
						close(fd[l][m][n]);

			dup2(fd[pipeNum][0][WRITE], STDOUT_FILENO);
			close(fd[pipeNum][0][WRITE]);
			
			posFile+=i;//localizacao da posicao do ficheiro nos argumentos do comando da shell
			execlp("tail", "tail", "-f", "-n", "0", argv[posFile], NULL);
		}
		
		//Novo processo
		pidFiles[1][i] = fork();
		if(pidFiles[1][i]<0){
			perror("Fork error.\n");
			return -1;
		}
		
		if(pidFiles[1][i] == 0){//segundo processo filho, realiza o grep
			pipeNum=i;
			int l,m,n;
			for(l=0;l<numOfFiles;l++)	//close dos pipes abertos
				for(m=0;m<2;m++)
					for(n=0;n<2;n++)
						if(l==pipeNum && m==0 && n==0) {}
						else if(l==pipeNum && m==1 && n==WRITE) {}
						else {close(fd[l][m][n]);};
		
			dup2(fd[pipeNum][0][READ], STDIN_FILENO);
			dup2(fd[pipeNum][1][WRITE], STDOUT_FILENO);
			close(fd[pipeNum][0][READ]);
			close(fd[pipeNum][1][WRITE]);
			
			execlp("grep", "grep", "--line-buffered", word, NULL);
		}
		
		//Novo processo
		pidFiles[2][i] = fork();		
		if(pidFiles[2][i]<0){
			perror("Fork error.\n");
			return -1;
		}
		
		if(pidFiles[2][i] == 0){//terceiro processo filho, mostra no ecrã quando encontra nova frase
			pipeNum=i;
			while(1){
				int l,m,n;
				for(l=0;l<numOfFiles;l++)	//ciclo que faz close da generalidade dos pipes
					for(m=0;m<2;m++) for(n=0;n<2;n++)
						if(!(l==pipeNum && m==1 && n==READ))
							close(fd[l][m][n]);
				
				read(fd[pipeNum][1][READ], line, 100);
				
				for(j=MAXLINE; j>0; j--) //coloca a ultima aspa no sitio certo
					if(line[j]!=0){
						line[j]='\"';
						break;}
				
				time_t t = time(NULL);
		 		struct tm tm = *localtime(&t);
				printf("\n%02d-%02d-%02dT%02d:%02d:%02d - %s - \"%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, argv[3+i], line);
				fflush(stdout);
			}
		}
	}
	
	int posFileDeleted[numOfFiles]; //array com os ficheiros que ja foram apagados
	for(i=0; i<numOfFiles; i++){
		posFileDeleted[i]=-1; //se ==-1 então o processo esta activo
	}
	int countRemFiles = 0;  //conta numero de ficheiros removidos
	
	while(running){
		now = clock();

		if((now-start)/CLOCKS_PER_SEC > timeTemp){
			timeTemp += 5; //faz contagem do tempo e verificacoes de 5 em 5 seg

			for(i=0; i<numOfFiles; i++) //verifica se algum dos ficheiros foi removido e mata o processo respetivo
				if(posFileDeleted[i]!=-1) continue;//este ficheiro esta a ser monitorizado?
				else if( access(argv[3+i], F_OK) == -1 ){ //ficheiro nao existe
					countRemFiles += 1;  //conta numero de ficheiros removidos, incrementa quando é removido

					kill(pidFiles[0][i], SIGUSR1); //mata os processos do ficheiro: tail (0), grep (1), visualizador de mensagens (2)
					kill(pidFiles[1][i], SIGUSR1);
					kill(pidFiles[2][i], SIGUSR1);
					
					printf("\nFile %s was removed.", argv[3+i]);
					fflush(stdout);
					posFileDeleted[i]=i; //a posicao do processo morto é gravada 
					if(countRemFiles == numOfFiles){
						running=0;
						printf("\n\nThere are no files available to monitor! Process will end.");
					}						
				}
		}
		if((now-start)/CLOCKS_PER_SEC == execTime){
			printf("\n\n...time's up! End of monitoring.");
			running=0;
		}
	}
		
	for(j=0;j<numOfFiles;j++){ //faz kill a todos os processos ativos
		for(i=0;i<3;i++){
			if(posFileDeleted[j]==-1){//processo ativo (-1)?
				pid_t pitKill = pidFiles[i][j];
				kill(pitKill, SIGUSR1);
			}
		}
	}
	
	printf("\n============================================\n\n");
	
	return 1;
}
