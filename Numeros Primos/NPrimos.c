#include "queue.h"

#define QUEUE_CAPACITY 10

unsigned int vMax, pindex, *primes, nt;
pthread_mutex_t mtnt, mtpr;
sem_t smain;
pthread_cond_t cvar = PTHREAD_COND_INITIALIZER;

int cmpfunc(const void * a, const void * b){
	return ( *(unsigned int*)a - *(unsigned int*)b );
}

void *thr_funcFilt(void *arg){
	CircularQueue* inQueue = (CircularQueue*)arg;
	
	int myPrime = queue_get(inQueue);
	if(myPrime > sqrt(vMax)){
		while(1){
			if(myPrime == 0){
				queue_destroy(inQueue);
				break;
			}
			pthread_mutex_lock(&mtpr);
				primes[pindex]=myPrime;
				pindex++;
			pthread_mutex_unlock(&mtpr);
			myPrime=queue_get(inQueue);
		}
		
		pthread_mutex_lock(&mtnt);
			while(nt != 0){
				pthread_cond_wait(&cvar, &mtnt);
			}
		pthread_mutex_unlock(&mtnt);
		
		sem_post(&smain);
	}
	else{
		pthread_mutex_lock(&mtnt);
			nt++;
		pthread_mutex_unlock(&mtnt);
		
		CircularQueue* outQueue;
		queue_init(&outQueue, QUEUE_CAPACITY);
		
		pthread_t tid;
		pthread_create(&tid, NULL, thr_funcFilt, outQueue);
		
		while(1){
			int num = queue_get(inQueue);
			if(num == 0){
				queue_destroy(inQueue);
				break;
			}
			else if(num%myPrime != 0){
				queue_put(outQueue, num);
			}
		}
		queue_put(outQueue, 0);
		
		pthread_mutex_lock(&mtpr);
			primes[pindex]=myPrime;
			pindex++;
		pthread_mutex_unlock(&mtpr);
		
		pthread_mutex_lock(&mtnt);
			nt--;
			pthread_cond_signal(&cvar);
		pthread_mutex_unlock(&mtnt);
	}
	
	return NULL;
}

void *thr_funcInit(void *arg){
	pindex = 0;
	primes[pindex] = 2;
	pindex++;
	
	if(vMax > 2){
		pthread_mutex_lock(&mtnt);
			nt++;
		pthread_mutex_unlock(&mtnt);
		
		CircularQueue* outQueue;
		queue_init(&outQueue, QUEUE_CAPACITY);
		
		pthread_t tid;
		pthread_create(&tid, NULL, thr_funcFilt, outQueue);
		
		int i;
		for(i = 3; i < vMax; i+=2){
			queue_put(outQueue, i);
		}
		queue_put(outQueue, 0);
		
		pthread_mutex_lock(&mtnt);
			nt--;
		pthread_mutex_unlock(&mtnt);
	}
	else{
		sem_post(&smain);
	}
	
	return NULL;
}


int main(int argc, char** argv){	
	vMax = atof(argv[1]);
	nt = 0;
	
	unsigned int espacoEstimado = (unsigned int) ceil(1.2*(vMax/log(vMax)));
	primes = (unsigned int *)malloc(espacoEstimado*(sizeof(unsigned int)));
	
	printf("=================================\n");
	printf("Valor limite: %d\n\n", vMax);
	
	sem_init(&smain, 0, 0);
	
	pthread_t tid;
	pthread_create(&tid, NULL, thr_funcInit, NULL);
	
	sem_wait(&smain);
	
	qsort(primes, pindex, sizeof(unsigned int), cmpfunc);
	
	printf("Numero de primos para o limite dado: %d\n\n", pindex);
	
	printf("Primes between 0 and %d are:\n{ %d",atoi(argv[1]),primes[0]);
	unsigned i;
	for(i=1;i<pindex;i++){
		printf(", %d",primes[i]);
	}
	printf("}\n");
	printf("=================================\n");
	
	//liberta espaco alocado dos primos ao terminar
	free(primes);
	
	return 0;	
}
