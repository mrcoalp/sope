#include "queue.h"

pthread_mutex_t mtnt, mtpr;

int data, nr;
sem_t empty, full;

void *producer(void *arg){
	printf("Producer running...\n");
	
	int i;

	for(i = 0; i < 120; i++){
		printf("A mandar: %d\n", i);
		queue_put(arg, i);
	}
	return NULL;
}

void *consumer(void *arg){
	printf("Consumer running...\n");
	
	int i;

	for(i = 0; i < 120; i++){
		printf("A receber: %lu\n", queue_get(arg));
	}

	return NULL;
}

int main(int argc, char **argv){
	pthread_t p, c;
	
	CircularQueue *fila;
	queue_init(&fila, 20);

	pthread_create(&p, NULL, producer, fila);
	pthread_create(&c, NULL, consumer, fila);

	pthread_join(p, NULL);
	pthread_join(c, NULL);
	
	queue_destroy(fila);

	return 0;
}
