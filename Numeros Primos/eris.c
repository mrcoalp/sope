#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){
	int size = atoi(argv[1]) - 1;
	int primes[size];

	int i;
	for(i  = 0; i < size; i++){
		primes[i] = 2+i;
	}

	int prime;

	int k;

	for(i  = 0; i < size; i++){
		if(primes[i] != 0){
			prime = primes[i];
			printf("Prime: %d\n", prime);
		}

		for(k  = 1; k < size; k++){
			if(primes[k]%prime == 0)
				primes[k] = 0;
		}
	}

	return 0;
}