#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdlib.h>
#include <semaphore.h>
#include <stdio.h>
#include <pthread.h>
#include <math.h>

typedef unsigned long QueueElem;

typedef struct {
	QueueElem *v;
	unsigned int capacity, first, last;
	sem_t empty, full;
	pthread_mutex_t mutex;
} CircularQueue;



void queue_init(CircularQueue **q, unsigned int capacity){
	*q = ( CircularQueue *) malloc(sizeof (CircularQueue));
	sem_init(&((*q)->empty), 0, capacity);
	sem_init(&((*q)->full), 0, 0);
	pthread_mutex_init(&((*q)->mutex), NULL);
	(*q)->v = (QueueElem *) malloc(capacity * sizeof (QueueElem));
	(*q)->capacity = capacity;
	(*q)->first = 0;
	(*q)->last = 0;
}

void queue_put(CircularQueue *q, QueueElem value){
	sem_wait(&(q->empty));
	
	pthread_mutex_lock(&(q->mutex));
		q->v[q->last] = value;
		q->last = (q->last + 1)%q->capacity;
	pthread_mutex_unlock(&(q->mutex));
	
	sem_post(&(q->full));
}

QueueElem queue_get(CircularQueue *q){
	QueueElem ret;
	
	sem_wait(&(q->full));
	
	pthread_mutex_lock(&(q->mutex));
		ret = q->v[q->first];
		q->first = (q->first + 1)%q->capacity;
	pthread_mutex_unlock(&(q->mutex));
	
	sem_post(&(q->empty));
 
	return ret;
}

void queue_destroy( CircularQueue *q){
	sem_destroy(&(q->empty));
	sem_destroy(&(q->full));
	free(q->v);
	free(q);
}

#endif
